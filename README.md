# Firmware - Git Lesson

A quick lesson of the essentials on git usage.

If you know how to use git, please do the following:

1.  Create an account so you can use GitLab
2.  Clone a copy of the repo
3.  Create your own branch named with the following syntax "firstName_lastName"
4.  Create a text file with the following syntax "Intro_firstName_LastName"
5.  Add it to your branch
6.  Add your name to the top of the file
7.  Commit with a message
8.  Add a sentence below it describing yourself and what EE stuff you like
9.  Commit with message
10.  Below the sentence, add your email and phone number
11.  Commit with message
12.  Create a Pull request and attempt to merge to the master

After that I'll review your changes and I'll merge it to the master. 

# I don't know git

If you're unfamiliar with git, I'll make a short video on this process. I'll 
also link some useful pages on what it is and why you use it. You will still 
have to do the steps shown in the first part. 

Trust me, it's worth learning. It will make your life easier and also impress
future employers...

Here is the video I made on how to complete this assignment. One thing I left
out of the video is since we are all adding files, use the the git pull command
to bring in the most recent files, otherwise you could have merge issues. For 
users of git, if you have a more elegant way, please suggest it in the slack
channel. 

https://youtu.be/DIwkmecis1I

Thanks